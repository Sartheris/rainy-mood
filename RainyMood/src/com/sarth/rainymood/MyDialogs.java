package com.sarth.rainymood;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MyDialogs {

	public static void showAboutDialog(Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.about_title)
			.setMessage(R.string.about_text);
		AlertDialog dialog = builder.create();
		dialog.setIcon(R.drawable.ic_launcher);
		
		dialog.show();
	}
	
	public static void showAutoChangeDialog(final Context context) {
		AlertDialog dialog;
		LayoutInflater inflater = LayoutInflater.from(context);
		View dialogView = inflater.inflate(R.layout.dialog_auto_change, null);
		final EditText et = (EditText) dialogView.findViewById(R.id.etMinutes);
		final CheckBox cb = (CheckBox) dialogView.findViewById(R.id.cbRandomize);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(R.string.dialog_auto_change_title)
			.setView(dialogView)
			.setPositiveButton(R.string.dialog_auto_change_button_set, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					int minutesToChangeImage = Integer.parseInt(et.getText().toString());
					if (minutesToChangeImage > 60) {
						minutesToChangeImage = 60;
					} else if (minutesToChangeImage < 1) {
						minutesToChangeImage = 1;
					}
					minutesToChangeImage *= 60000;
					MyPrefs.setTimeToNextImage(minutesToChangeImage);
					MyPrefs.setRandomImageChecked(cb.isChecked());
					dialog.dismiss();
					Toast.makeText(context, R.string.dialog_auto_change_confirmation, Toast.LENGTH_LONG).show();
				}
			})
			.setNegativeButton(android.R.string.cancel, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
		dialog = builder.create();
		dialog.setIcon(R.drawable.ic_launcher);
		dialog.show();
	}
}
