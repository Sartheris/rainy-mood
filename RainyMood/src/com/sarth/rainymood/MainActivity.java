package com.sarth.rainymood;

import java.util.Random;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final int MAX_IMAGES = 3;

	private MediaPlayer musicPlayer;
	private int length;
	private int currentImage;
	private long timeToNextImage;
	private ImageView iv;
	private AnimationDrawable animation;
	private CountDownTimer timer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initViews();
		MyPrefs.loadSharedPreferences(this);
		Toast.makeText(this, R.string.hint_toast, Toast.LENGTH_LONG).show();
		if (MyPrefs.isAutoChangeChecked()) {
			timeToNextImage = MyPrefs.getTimeToNextImage();
			countTimeToNextImage();
		}
	}
	
	private void countTimeToNextImage() {
		Log.v("", "time to next image: " + timeToNextImage);
		timer = new CountDownTimer(timeToNextImage, 1000) {
			
			@Override
			public void onTick(long arg0) {				
			}
			
			@Override
			public void onFinish() {
				if (MyPrefs.isRandomImageChecked()) {
					int i = new Random().nextInt(MAX_IMAGES + 1);
					chooseImage(i);
					Log.v("", "new image chosen: " + i);
				} else {
					setImage(1);
				}
			}
		};
		timer.start();
	}

	private void initMediaPlayer() {
		musicPlayer = MediaPlayer.create(this, R.raw.rainy);
		musicPlayer.setLooping(true);
		musicPlayer.seekTo(length);
		// TODO da se zapo4va s plavno uveli4avane na zvuka
		musicPlayer.start();
	}

	@Override
	public void openOptionsMenu() {
		super.openOptionsMenu();
		Configuration config = getResources().getConfiguration();
		if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) > Configuration.SCREENLAYOUT_SIZE_LARGE) {
			int originalScreenLayout = config.screenLayout;
			config.screenLayout = Configuration.SCREENLAYOUT_SIZE_LARGE;
			super.openOptionsMenu();
			config.screenLayout = originalScreenLayout;
		} else {
			super.openOptionsMenu();
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		// animation.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		musicPlayer.pause();
		length = musicPlayer.getCurrentPosition();
	}

	@Override
	protected void onResume() {
		super.onResume();
		setImage(0);
		initMediaPlayer();
		initAnimation();
	}

	private void initAnimation() {
		currentImage = MyPrefs.getImage();
		// animation = (AnimationDrawable) iv.getBackground();
		// animation.start();
	}

	private void initViews() {
		iv = (ImageView) findViewById(R.id.imageView1);
		iv.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				openOptionsMenu();
				return false;
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.my_menu, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
	    menu.findItem(R.id.auto_change).setChecked(MyPrefs.isAutoChangeChecked());
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.previous:
			setImage(-1);
			break;
		case R.id.next:
			setImage(1);
			break;
		case R.id.auto_change:
			item.setChecked(!item.isChecked());
			MyPrefs.setAutoChangeChecked(item.isChecked());
			if (item.isChecked()) {
				MyDialogs.showAutoChangeDialog(this);
			}
			// TODO kato se zatvori dialoga, da vru6ta nqkade v aktivitito, 4e moje da se startira taimera
			break;
		case R.id.about:
			MyDialogs.showAboutDialog(this);
			break;
		case R.id.exit:
			finish();
			break;
		}
		return false;
	}

	private void setImage(int direction) {
		currentImage = currentImage + direction;
		if (currentImage < 0) {
			currentImage = MAX_IMAGES;
		} else if (currentImage > MAX_IMAGES) {
			currentImage = 0;
		}
		chooseImage(currentImage);
	}

	private void chooseImage(int image) {
		// TODO da se setne plavno smenqne s fade out
		switch (image) {
		case 0:
			// Rain
			iv.setImageResource(R.drawable.desert_1);
			break;
		case 1:
			// Desert
			iv.setImageResource(R.drawable.desert_2);
			break;
		case 2:
			// Beach
			iv.setImageResource(R.drawable.desert_3);
			break;
		case 3:
			// Forest
			iv.setImageResource(R.drawable.desert_4);
			break;
		default:
			break;
		}
		MyPrefs.setImage(image);
	}
}